from conans import ConanFile, tools
import shutil

class CppTaskflowConan(ConanFile):
    name           = "taskflow"
    version        = "2.5.0"
    settings       = "os", "compiler"
    license        = "MIT"
    url            = "https://bitbucket.org/toge/conan-cpp-taskflow/"
    homepage       = "https://cpp-taskflow.github.io"
    description    = "Modern C++ Parallel Task Programming Library"
    no_copy_source = True
    topics         = ("task", "parallel")

    def source(self):
        if self.version < "2.5.0":
            zip_name = "cpp-taskflow-v{}.zip".format(self.version)
            tools.download("https://github.com/cpp-taskflow/cpp-taskflow/archive/v{}.zip".format(self.version), zip_name)
            tools.unzip(zip_name)
            shutil.move("cpp-taskflow-" + self.version, "cpp-taskflow")
            os.unlink(zip_name)
        else:
            tools.get("https://github.com/cpp-taskflow/cpp-taskflow/archive/{}.zip".format(self.version))
            shutil.move("taskflow-" + self.version, "taskflow")
            
    def configure(self):
        compiler = str(self.settings.compiler)
        if self.settings.compiler.cppstd:
            tools.check_min_cppstd(self, "17")
        else:
            self.output.warn("%s recipe lacks information about the %s compiler standard version support" % (self.name, compiler))

    def package(self):
        if self.version < "2.5.0":
            self.copy("*.hpp", src="cpp-taskflow/taskflow", dst="include/taskflow", keep_path=True)
        else:
            self.copy("*.hpp", src="taskflow/taskflow", dst="include/taskflow", keep_path=True)

    def package_id(self):
        self.info.header_only()

    def package_info(self):
        if self.settings.os == "Linux":
            self.cpp_info.libs = ["pthread"]
